<?php
/**
 * Function that returns the file corresponding to the difficulty
 * @param $difficulty difficulty chosen
 * @return array the file.
 */
function receiveFromForm($difficulty) {
    $filename = "./datas/map/" . $difficulty . ".txt";
    $mapArray = array();
    if (file_exists($filename)) {
        $desc = fopen($filename, "r");
        while ($line = trim(fgets($desc))) {
            $lineArray = array_map('intval', str_split($line));
            array_push($mapArray, $lineArray);
        }
    }
    return $mapArray;
}
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Tower Defense</title>
        <meta charset="UTF-8"/>
        <link rel="stylesheet" type="text/css" href="css/game.css"/>
        <link rel="stylesheet" type="text/css" href="css/global.css">
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css'>
        <link rel="stylesheet" href="css/aside.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <script src="js/printCanvas.js"></script>
        <script src="js/aside.js"></script>
    </head>
    <body>
    <button class="menu" id="menu-on"><i class="fa fa-bars" aria-hidden="true"></i></button>
    <button class="menu" id="menu-off"><i class="fa fa-bars" aria-hidden="true"></i></button>

    <aside>
        <h3 class="aside-title">Tower Defense</h3>
        <h4 class="aside-subtitle"> <?php echo $_POST['level']; ?></h4>
        <a href="home.html"  class="aside-back">Back to main menu</a>
        <table id="turret-table">
            <tr>
                <td><img id="turret1" src="datas/img/turret1.png" alt="Turret1" onclick="setTurretType(0)"/></td>
                <td class="turret-desc">Power : 20<br/>Scope : 40<br/>Price : 50</td>
            </tr>
            <tr>
                <td><img id="turret2" src="datas/img/turret2.png" alt="Turret2" onclick="setTurretType(1)"/></td>
                <td class="turret-desc">Power : 50 <br/>Scope : 60 <br/>Price: 75</td>
            </tr>
        </table>
        <p class="aside-bottom">Developed by Clément Drouin and Gabriel Pellerin</p>
    </aside>
    <div class="info">
        <p>
            <span id="life-text"> Life  : </span>
            <span id="life-info">100</span>
            <img src="datas/img/life.png" alt="life"/>
        </p>
        <p>
            <span id="money-text">Money : </span>
            <span id="money-info">0</span>
            <img src="datas/img/money.png" alt="money"/>
        </p>
        <p>
            <span id="kill-text">Kills : </span>
            <span id="kill-info">0</span>
            <img src="datas/img/enemy_bot.png" alt="enemy"/>
        </p>
    </div>
        <?php
        if (isset($_POST['map'])) {
            echo "<canvas id=\"plate\" width=\"600\" height=\"600\"></canvas>";
            $mapArray = receiveFromForm($_POST['map']);
            echo "<script>";
            echo "var array = new Array();";
            // Translation of the array
            echo "var array =".json_encode($mapArray).";";
            echo "window.onload = function () {";
            echo "initialize(array);";
            echo "addAsideEvent();";
            echo "}";
            echo "</script>";
        } else {
            header('Location: ./home.html');
            exit();
        }
        ?>
    </body>
</html>
