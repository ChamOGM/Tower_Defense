/**
 * We have at the beginning a form that we make hidden.
 */
window.onload = function () {
    document.getElementById("mapInput").style.visibility = 'hidden';
    document.getElementById("level").style.visibility = 'hidden';
    createMapListener();
};

/**
 * We create the level chooser and fill the form values on level click.
 */
function createMapListener() {
    var maps = ["easy", "normal", "hard"];
    var level = ["Lullaby (EASY)", "Sweet dreams (NORMAL)", "Highway to Hell (Hard)"];
    var element= document.getElementsByClassName('map');
    for(var i=0; i < element.length; i++){
       element[i].addEventListener("click", submit(maps[i], level[i]), false);
    }
}

/**
 * Function that fills the invisible form, and submit the choice to server to launch the game.
 * @param map
 * @param level
 * @returns {Function}
 */
function submit(map, level) {
    return function() {
        document.getElementById("mapInput").value = map;
        document.getElementById("level").value = level;
        document.getElementById("menuForm").submit();
    };
}

