function addAsideEvent() {
    document.getElementById('menu-off').style.visibility = 'hidden';
    document.getElementById('menu-on').addEventListener("click", menuOn, false);
    document.getElementById('menu-off').addEventListener("click", menuOff, false);
}

function menuOn() {
    document.getElementsByTagName('aside')[0].style.visibility = 'visible';
    document.getElementById('menu-on').style.visibility = 'hidden';
    document.getElementById('menu-off').style.visibility = 'visible';
}

function menuOff() {
    document.getElementsByTagName('aside')[0].style.visibility = 'hidden';
    document.getElementById('menu-on').style.visibility = 'visible';
    document.getElementById('menu-off').style.visibility = 'hidden';
}