function Turret1(x, y) {
    this.x = parseInt(x);
    this.y = parseInt(y);
    this.imgsrc = "./datas/img/turret1.png";
    this.width = 5;
    this.frequency = 400;
    this.lastShot = 0;
    this.cvs = document.createElement('canvas');
    this.ctx = this.cvs.getContext('2d');
}

function Turret2(x, y) {
    this.x = parseInt(x);
    this.y = parseInt(y);
    this.imgsrc = "./datas/img/turret2.png";
    this.width = 5;
    this.frequency = 1000;
    this.lastShot = 0;
    this.cvs = document.createElement('canvas');
    this.ctx = this.cvs.getContext('2d');
}

Turret1.prototype.power = 20;
Turret1.prototype.scope = 50;
Turret1.prototype.cost  = 50;
Turret1.prototype.angle = Math.PI;

Turret2.prototype.power = 50;
Turret2.prototype.scope = 60;
Turret2.prototype.cost  = 100;
Turret2.prototype.angle = Math.PI;

function printTurret(t, ctx) {
    var img = new Image();
    img.src = t.imgsrc;
    t.cvs.width = img.width;
    t.cvs.height = img.height;
    t.ctx.translate(img.width / 2, img.height / 2);
    t.ctx.rotate(-t.angle);
    t.ctx.drawImage(img, -img.width / 2, -img.height / 2);
    ctx.drawImage(t.cvs, t.x - ZONE_WIDTH / 2, t.y - ZONE_HEIGHT / 2);
}

function manageTurret() {
    var allDead = player.turrets.length > 0;
    for (var j = 0; j < player.turrets.length; ++j) {
        var turret = player.turrets[j];
        var alreadyShot = false;
        for (var i = 0; i < enemies.length; ++i) {
            var e = enemies[i];
            if (e != null) {
                //console.log("turret : " + turret.x + " " + turret.y + " |||| enemy : " + e.x + " " + e.y);
                if (turret.lastShot === 0 && !alreadyShot && inArea(e.y, e.x, turret)) {
                    turret.angle = -Math.atan2(e.y - turret.y, e.x - turret.x);
                    e.removeLife(turret.power);
                    if (!e.isAlive()) {
                      player.money += 10;
                      ++player.nkilled;
                      updateMoney();
                      updateKills();
                      enemies.splice(i, 1);
                      --i;
                    }
                    alreadyShot = true;
                }
                /*if (turret.lastShot == 0 && alreadyShot) {
                    turret.lastShot += REFRESH_RATE;
                } else if (turret.lastShot != 0) {
                    turret.lastShot = turret.lastShot + REFRESH_RATE >= turret.frequency ? 0 : turret.lastShot + REFRESH_RATE;
                }*/
                if (e.isAlive()) {
                    allDead = false;
                }
            }
        }
        if (turret.lastShot === 0 && alreadyShot || turret.lastShot !== 0) {
            turret.lastShot = turret.lastShot + REFRESH_RATE >= turret.frequency ? 0 : turret.lastShot + REFRESH_RATE;
        }
    }
    if (player.nkilled > 300 || player.life === 0) {
        alert("Fin de la partie !");
        clearInterval(turretIntervalId);
        clearInterval(enemyIntervalId);
        clearInterval(spawnIntervalId);
    }

}

function canPutTurret() {
  if (mouse.x < 0 || mouse.y < 0 || mouse.x >= canvas.width || mouse.y >= canvas.height) {
    return false;
  }
  if (getPosition(mouse.y, mouse.x) != null && getPosition(mouse.y, mouse.x).path) {
    return false;
  }
  
  for (var i = 0; i < player.turrets.length; ++i) {
    var turret = player.turrets[i];
    if (inArea(mouse.y, mouse.x, turret)) {
      return false;
    }
  }
  return true;
}

function inArea(i, j, turret) {
  return Math.sqrt(Math.pow(i - turret.y, 2) + Math.pow(j - turret.x, 2))
            <= turret.scope;
}

/**
 * Set the turret type.
 * @param type the index of the turret constructor array.
 */
function setTurretType(type) {
    t = type;
}