/**
 * Player instance definition.
 *
 * @param life the base life.
 * @param money the base money.
 * @constructor
 */
function Player(life, money) {
    this.life = life;
    this.money = money;
    this.turretTypes = [];
    this.turrets = [];

    this.setTurretTypes = function() {
        this.turretTypes[0] = Turret1;
        this.turretTypes[1] = Turret2;
    };

    this.setTurretTypes();
    this.nkilled = 0;
    this.addTurret = function(turret) {
        if (typeof turret !== undefined) {
            this.turrets.push(turret);
            this.money -= turret.cost;
        }
    }
}
