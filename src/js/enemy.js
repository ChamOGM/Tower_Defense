/**
 * Constructor of the base enemy.
 *
 * @param x horizontal position
 * @param y vertical position
 * @param width the width of the enemy (to be touched by turrets)
 * @param direction base direction
 * @param speed speed by image.
 * @constructor
 */
function Enemy1(x, y, width, direction, speed) {
    this.x = parseInt(x);
    this.y = parseInt(y);
    this.direction = direction;
    this.speed = speed;
    this.dx = 0;
    this.dy = 0;
    this.width = width;
    this.imgsrc = null;
    this.borderLimit = 20; // It will change direction 20 px before touching the border
    changeEnemyImage(this, this.direction);
    this.manageSpeed = function() {
        this.dx = 0;
        this.dy = 0;
        // WEST
        if (this.direction === 0) {
            this.dx = -this.speed;
        // EAST
        } else if (this.direction === 1) {
            this.dx = this.speed;
        // NORTH
        } else if (this.direction === 2) {
            this.dy = -this.speed;
        // SOUTH
        } else if (this.direction === 3) {
            this.dy = this.speed;
        }
    };

    this.manageSpeed();

    /**
     * Is the enemy in the battlefield ?
     * @returns {boolean} true if it's in the battlefield, else false.
     */
    this.inBattlefield = function() {
        return this.x < battlefield[0].length * ZONE_WIDTH && this.x >= 0 &&
            this.y < battlefield.length * ZONE_HEIGHT && this.y >= 0;
    };

    this.isAlive = function() {
        return this.life > 0;
    };

    /**
     * Principal method of the class.
     *
     * Manages if the enemy shot a border, changes direction in that case, else it just goes.
     */
    this.move = function() {
        this.x += this.dx;
        this.y += this.dy;
        if (!this.inBattlefield() || !getEnemyPosition(this).path) {
            this.x -= this.dx;
            this.y -= this.dy;
            var pos = getEnemyPosition(this);
            var found = false;
            for (var i = 0; i < 4 && !found; ++i) {
                var zone  = testZone(this, pos, i);
                if (zone != null) {
                    found = true;
                    this.direction = i;
                    changeEnemyImage(this, this.direction);
                    this.manageSpeed();
                }
            }
            if (!found) { // we are on terminus
                player.life -= 10;
                updateLife();
                this.life = 0;
            }
        }
    };

    this.removeLife = function(hit) {
        this.life -= hit;
    }
}
Enemy1.prototype.life = 100;
Enemy1.prototype.baseLife = 100;

function Enemy2(x, y, width, life, direction, speed) {
    Enemy1.call(this, x, y, width, life, direction, speed);
}

Enemy2.prototype = Object.create(Enemy1.prototype);
Enemy2.prototype.constructor = Enemy2;
Enemy2.prototype.life = 1000;
Enemy2.prototype.baseLife = 1000;

/**
 * Function that changes the imgsrc enemy parameter hence its direction.
 *
 * @param enemy the enemy to modify the image.
 * @param direction the direction.
 */
function changeEnemyImage(enemy, direction) {
    var src = "./datas/img/enemy_";
    if (enemy.constructor === Enemy2) {
        src += "tank_";
    }
    switch (direction) {
        case 0:
            enemy.imgsrc = src + "left.png";
            break;
        case 1:
            enemy.imgsrc = src + "right.png";
            break;
        case 2:
            enemy.imgsrc = src + "top.png";
            break;
        case 3:
            enemy.imgsrc = src + "bot.png";
            break;
        default:
            console.log('problème changement image');
    }
}

/**
 * Function that test if an enemy is in a walkable zone or not, and returns the current zone.
 * @param enemy the enemy
 * @param zone the current zone the enemy is.
 * @param direction the direction the enemy goes.
 * @returns {null} if there is nos possible zone, the next zone else.
 */
function testZone(enemy, zone, direction) {
    var z;

    if (direction === enemy.direction) {
        return null;
    }

    if (direction === 0 && enemy.direction !== 1) {
        if (zone.x === 0) {
            return null;
        }
        z = battlefield[zone.y][zone.x - 1];
        return z.path ? z : null;

    } else if (direction === 1 && enemy.direction !== 0) {
        if (zone.x === battlefield[zone.y].length - 1) {
            return null;
        }
        z = battlefield[zone.y][zone.x + 1];

        return z.path ? z : null;

    } else if (direction === 2 && enemy.direction !== 3) {
        if (zone.y === 0) {
            return null;
        }
        z = battlefield[zone.y - 1][zone.x];

        return z.path ? z : null;

    } else if (direction === 3 && enemy.direction !== 2) {
        if (zone.y === battlefield.length - 1) {
            return null;
        }
        z = battlefield[zone.y + 1][zone.x];

        return z.path ? z : null;
    }

    return null;
}

/**
 * Function that returns the enemy position at a given state.
 *
 * @param enemy the enemy we want the position
 * @returns {*} the zone the enemy is.
 */
function getEnemyPosition(enemy) {
    if (enemy.direction === 0) {
        return getPosition(parseInt("" + enemy.y), parseInt("" + enemy.x) - enemy.borderLimit);
    } else if (enemy.direction === 1) {
        return getPosition(parseInt("" + enemy.y), parseInt("" + enemy.x) + enemy.borderLimit);
    } else if (enemy.direction === 2) {
        return getPosition(parseInt("" + enemy.y) - enemy.borderLimit, parseInt("" + enemy.x));
    } else {
        return getPosition(parseInt("" + enemy.y) + enemy.borderLimit, parseInt("" + enemy.x));
    }
}

/**
 * Function that returns the zone corresponding to horizontal and vertical points.
 * @param i the vertical position
 * @param j the horizontal position.
 */
function getPosition(i, j) {
  return battlefield[parseInt(i / ZONE_HEIGHT)][parseInt(j / ZONE_WIDTH)];
}

/**
 * Function launched by interval. Each time it draws enemies and move them.
 * If they are on terminus the player lose 10 life points.
 * @param array the battlefield
 * @returns {boolean}
 */
function manageEnemy(array) {
    printBattlefield(array);
    for (var i = 0; i < enemies.length; ++i) {
        if (enemies[i] != null) {
            var img = new Image();
            img.src = enemies[i].imgsrc;
            ctx.drawImage(img, enemies[i].x - ZONE_WIDTH / 2, enemies[i].y - ZONE_HEIGHT / 2);
            ctx.fillStyle = 'red';
            ctx.fillRect(enemies[i].x - ZONE_WIDTH / 2 + 11, enemies[i].y - ZONE_HEIGHT / 2, 30 * enemies[i].life
                / enemies[i].baseLife, 5);

            enemies[i].move();
            var currentX = getEnemyPosition(enemies[i]).x;
            var currentY = getEnemyPosition(enemies[i]).y;
            var isArr = isArrival(array, currentY, currentX);
            if (isArr || !enemies[i].isAlive()) {
                if (isArr) {
                    player.life -= 10;
                    updateLife();
                }
                enemies[i] = null;
                return true;
            }
        }
    }
}

/**
 * Function that is called iteratively.
 * Each time it creates a new enemy.
 */
function manageSpawn() {
    if (player.nkills > 100 && Math.random() * 100 > 50 || Math.random() * 100 < 10) {
        enemies.push(new Enemy2(departure.x * ZONE_WIDTH, departure.y * ZONE_HEIGHT, 10, 1, 1));
    } else {
        enemies.push(new Enemy1(departure.x * ZONE_WIDTH, departure.y * ZONE_HEIGHT, 10, 1, 3));
    }
    if (player.nkilled > 100) {
        Enemy1.prototype.baseLife = 300;
        Enemy1.prototype.life = 300;
    }
}
