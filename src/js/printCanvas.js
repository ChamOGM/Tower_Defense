// import of the scripts.
var scripts = ["./js/turret.js", "./js/enemy.js", "./js/player.js"];
for (var script in scripts) {
    var imported = document.createElement('script');
    imported.src = scripts[script];
    document.head.appendChild(imported);
}

// global variables.
var ZONE_WIDTH = 48;
var ZONE_HEIGHT = 48;
var FPS = 30;
var REFRESH_RATE = 1000 / FPS;
var battlefield = [];
var enemies = [];
var t = 0;
var departure;
var canvas;
var ctx;
var spawnIntervalId;
var enemyIntervalId;
var turretIntervalId;
var mouse;
var player;



/**
 * Function that sets the mouse Position each time the cursor moves.
 * @param evt
 */
function getMousePosition(evt) {
    var rect = canvas.getBoundingClientRect();
    mouse = {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

/**
 * Function that refreshes the money value the player has.
 */
function updateMoney() {
  document.getElementById("money-info").innerHTML = player.money + "";
}

/**
 * Function that refreshes the player remaining life.
 */
function updateLife() {
  document.getElementById("life-info").innerHTML = player.life + "";
}

/**
 * Function that refreshes the number of kills the player did.
 */
function updateKills() {
  document.getElementById("kill-info").innerHTML = player.nkilled + "";
}

/**
 * Base function of the file. It initializes the plate, and start the different managing calls.
 * @param array the base plate, composed of number.
 */
function initialize(array) {
    player = new Player(100, 200); // life, base money

    canvas = document.getElementById("plate");
    canvas.width = array[0].length * ZONE_WIDTH;
    canvas.height = array.length * ZONE_HEIGHT;
    updateMoney();
    // listener on mousedown to put turret here.
    canvas.addEventListener('mousedown', function() {
        if (player.money >= player.turretTypes[t].prototype.cost && canPutTurret(mouse.y, mouse.x)) {
          player.addTurret(new player.turretTypes[t](mouse.x, mouse.y));
          updateMoney();
        }
    });
    // Listener on mousemove.
    window.addEventListener('mousemove', getMousePosition, false);
    ctx = canvas.getContext("2d");
    // We create the battlefield ("converting" it in Zone)
    createBattlefield(array);
    // First print of the battlefield.
    printBattlefield(array);

    // it's the global variable which is the departure Zone.
    departure = findDeparture(battlefield, array);
    enemies[0] = new Enemy1(departure.x * ZONE_WIDTH + 15, departure.y * ZONE_HEIGHT + 15, 10, 1, 3);

    // Different intervaled calls.
    spawnIntervalId = setInterval(manageSpawn, 2000);
    enemyIntervalId = setInterval(manageEnemy, REFRESH_RATE, array);
    turretIntervalId = setInterval(manageTurret, REFRESH_RATE, array);
}

/**
 * Constructor of the zone.
 * @param x horizontal position.
 * @param y vertical position.
 * @param path true if it's a walkable zone, else false.
 * @constructor
 */
function Zone(x, y, path) {
    this.x = x;
    this.y = y;
    this.path = path;
}

// Functions that return true if the zone in the i - j positions are paths, departure or arrival.
function isPath(array, i, j) {
    return array[i][j] === 1 || array[i][j] === 2 || array[i][j] === 3;
}

function isDeparture(array, i, j) {
    return array[i][j] === 2;
}

function isArrival(array, i, j) {
    return array[i][j] === 3;
}

/**
 * Function that returns the departure zone.
 * @param battlefield the battledield
 * @param array
 * @returns {*}
 */
function findDeparture(battlefield, array) {
    for (var i = 0; i < array.length; ++i) {
        for (var j = 0; j < array[0].length; ++j) {
            if (isDeparture(array, i, j)) {
                return battlefield[i][j];
            }
        }
    }
}

/**
 * Function that creates the battlefield.
 * @param array the base array.
 */
function createBattlefield(array) {
    for (var i = 0; i < array.length; ++i) {
        battlefield[i] = [];
        for (var j = 0; j < array[i].length; ++j) {
            battlefield[i][j] = new Zone(j, i, isPath(array, i, j));
        }
    }
}

/**
 * Function that prints the battlefield.
 * @param array
 */
function printBattlefield() {
    for (var i = 0; i < array.length; ++i) {
        for (var j = 0; j < array[i].length; ++j) {
            var z = battlefield[i][j];
            var img = new Image();
            img.src = getGroundImage(array, i, j);
            ctx.drawImage(img, z.x * ZONE_WIDTH, z.y * ZONE_HEIGHT);
            if (isDeparture(array, i, j)) {
                img = new Image();
                img.src = getSpawnImage(array, i, j);
                ctx.drawImage(img, z.x * ZONE_WIDTH , z.y * ZONE_HEIGHT);
            } else if (isArrival(array, i, j)) {
                img = new Image();
                img.src = getExitImage(array, i, j);
                ctx.drawImage(img, z.x * ZONE_WIDTH, z.y * ZONE_HEIGHT);
            }
        }
    }
    for (i = 0; i < player.turrets.length; ++i) {
        printTurret(player.turrets[i], ctx);
    }
    drawMouse();
}

/**
 * Function that returns the image path of the zone.
 *
 * @param array
 * @param i
 * @param j
 * @returns {string}
 */
function getGroundImage(array, i, j) {
    var path = "./datas/img/";
    if (isPath(array, i, j)) {
        return path + "dirt.png";
    } else{
        path += "grass";
    }

    if (i > 0) {
        if (isPath(array, i - 1, j))
            path += "_top";
    }
    if (i + 1 < array.length) {
        if (isPath(array, i + 1, j))
            path += "_bot";
    }

    if (j > 0) {
        if (isPath(array, i, j - 1))
            path += "_left";
    }

    if (j + 1 < array[0].length) {
        if (isPath(array, i, j + 1))
            path += "_right";
    }

    if (path.indexOf("_") > -1) {
        return path + ".png";
    }

    if (i > 0 && j > 0) {
        if (isPath(array, i - 1, j - 1))
            return path + "_top_left_corner.png";
    }

    if (i + 1 < array.length && j > 0) {
        if (isPath(array, i + 1, j - 1))
            return path + "_bot_left_corner.png";
    }

    if (i > 0 && j + 1 < array[0].length) {
        if (isPath(array, i - 1, j + 1))
            return path + "_top_right_corner.png";
    }

    if (i + 1 < array.length && j + 1 < array[0].length) {
        if (isPath(array, i + 1, j + 1))
            return path + "_bot_right_corner.png";
    }
    return path + ".png";
}

/**
 * Function that returns the departure image path of the zone.
 *
 * @param array
 * @param i
 * @param j
 * @returns {string}
 */
function getSpawnImage(array, i, j) {
    var path = "./datas/img/spawn_";
    if (i > 0) {
        if(isPath(array, i - 1, j)) {
            path += "top.png";
        }
    }

    if (i + 1 < array.length) {
        if(isPath(array, i + 1, j)) {
            path += "bot.png";
        }
    }

    if (j > 0) {
        if(isPath(array, i, j - 1)) {
            path += "left.png";
        }
    }

    if (j + 1 < array.length) {
        if (isPath(array, i, j + 1)) {
            path += "right.png"
        }
    }
    return path;
}

/**
 * Function that returns the exit image path of the zone.
 * @param array
 * @param i
 * @param j
 * @returns {string}
 */
function getExitImage(array, i, j) {
    var path = "./datas/img/exit_";
    if (i > 0) {
        if(isPath(array, i - 1, j)) {
            path += "bot.png";
        }
    }

    if (i + 1 < array.length) {
        if(isPath(array, i + 1, j)) {
            path += "top.png";
        }
    }

    if (j > 0) {
        if(isPath(array, i, j - 1)) {
            path += "right.png";
        }
    }

    if (j + 1 < array.length) {
        if (isPath(array, i, j + 1)) {
            path += "left.png"
        }
    }
    return path;
}

/**
 * Function that draws a circle around the cursor, with the width of the selected turret,
 * and is red if it's not possible to put a turret here.
 */
function drawMouse() {
    if (!mouse) {
        return;
    }
    var range = player.turretTypes[t].prototype.scope;
    ctx.beginPath();
    ctx.globalAlpha = 0.2;
    ctx.arc(mouse.x, mouse.y, range, 0, 2 * Math.PI);
    if (canPutTurret(mouse.x, mouse.y) && player.money >= player.turretTypes[t].prototype.cost) {
        ctx.fillStyle = 'green';
    } else {
        ctx.fillStyle = 'red';
    }
    ctx.fill();
    ctx.globalAlpha = 1;
}
